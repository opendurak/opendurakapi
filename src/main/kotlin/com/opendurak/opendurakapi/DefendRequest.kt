package com.opendurak.opendurakapi

data class DefendRequest(
    val defendingCard: Card,
    val attackingCard: Card,
)
