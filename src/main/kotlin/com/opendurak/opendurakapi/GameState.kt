package com.opendurak.opendurakapi

data class GameState(
    val selfPlayer: PlayerWithOpenCards?,
    val players: List<Player>,
    val stackCount: Int,
    val trump: Card,
    val battlefield: List<Pair<Card, Card?>>,
    val attackerHash: String,
    val defenderHash: String,
    val helperHash: String?,
    val attackGaveUp: Boolean,
    val defenderGaveUp: Boolean,
    val helperGaveUp: Boolean,
    val gameFinished: Boolean,
)